require 'yaml'
require 'elvanto'
require 'handlebars'
require 'pp'
require 'vcardigan'
$config = YAML.load_file('../secrets.yml')
def getAllGroups(category_id)
    groups = ElvantoAPI.call("groups/getAll", {
        "page" => 1,
        "page_size" => 1000,
        "category_id" => category_id,
        "fields" => [
            "people",
        ]
    })
    g = groups['groups']['group']
    #pp p
    return g
end


def moreData(groups)
  ret = []
  groups.each do |group|
    data = {
        :name => group['name'],
        :description => proc {Handlebars::SafeString.new(group['description'])},
    };
    if group["people"].respond_to?('has_key?') and group["people"]["person"].any?
      data[:people] = group['people']['person'].sort_by { |person| person['lastname'] + '_'+ person['firstname'] }
      data[:leader] = group['people']['person'].select {|person| person['position'] == "Leader"}
    end
    ret << data
  end
  ret.sort_by {|group| group[:name]}
end

def main()
    ElvantoAPI.configure({:api_key=>$config['api_key']})
    groups = moreData(getAllGroups($config['dienst_category_id']))

    handlebars = Handlebars::Context.new
    template = handlebars.compile(File.read('./long.html'))
    data = {
        "groups" => groups,
    }
    File.write('long_out.html', template.call(data))

end
main()
