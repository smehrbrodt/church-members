#!/usr/bin/env bash
compass compile
ruby create.rb
wkhtmltopdf --margin-bottom 15mm --margin-left 10mm --margin-right 10mm --margin-top 15m --page-size A5  long_out.html long.pdf
