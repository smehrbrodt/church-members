#!/usr/bin/env bash
set -e

compass compile
ruby create.rb -t ./template_debug.html
wkhtmltopdf --margin-bottom 15mm --margin-left 10mm --margin-right 10mm --margin-top 15m --page-size A4  out.html gemeindelist_entwurf.pdf
