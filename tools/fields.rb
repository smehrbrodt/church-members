require 'yaml'
require 'elvanto'
require 'handlebars'
require 'pp'
$config = YAML.load_file('./secrets.yml')

def main()
  ElvantoAPI.configure({:api_key=>$config['api_key']})
  people = ElvantoAPI.call("people/customFields/getAll");

  pp people
end
main()
