require 'yaml'
require 'elvanto'
require 'handlebars'
require 'pp'
$config = YAML.load_file('./secrets.yml')

def getAllPeople
  people = ElvantoAPI.call("people/getAll", {
      "page" => 1,
      "page_size" => 1000,
      "category_id" => $config['category_id'],
      "fields" => [
          "gender",
          "birthday",
          "home_address",
          "home_city",
          "home_postcode",
          "marital_status",
      ]
  })

  p = people['people']['person']


  return p
end


def main()
  ElvantoAPI.configure({:api_key => $config['api_key']})
  start = 'r'
  finish = 'z'

  people = getAllPeople()
  people = people.select { |people| people['lastname'].downcase >= start and people['lastname'].downcase <= finish }
  people = people.sort_by { |a| a['lastname'] }

  people.each do |person|
    puts person['firstname'] + ' ' + person['lastname']

  end

end


main()
