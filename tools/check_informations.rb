require 'yaml'
require 'elvanto'
require 'handlebars'
require 'pp'
require 'time'
require_relative '../lib/elvanto-lib'
def main
  $config = YAML.load_file('../secrets.yml')
  ElvantoAPI.configure({:api_key => $config['api_key']})

  people = extended_data(people: get_people($config['category_id']), disable_children_age: false)
  #people = people.concat(getAllPeople($config['besucher_category_id']))
  people = people.sort_by {|person| person['lastname']}

  people.each do |person|
    puts "--- #{person['firstname']} #{person['lastname']} ---\n"
    puts "Hallo #{person['firstname']}, wir sind dabei die Daten aus der Gemeindeliste zu überprüfen. Stimmen die folgenden Angaben?\n"
    puts "Adresse: #{person['home_address']}, #{person['home_postcode']} #{person['home_city']}"
    if not person['birthday'].empty?
      puts "Geburtstag: #{person['birthday']}"
    else
      puts "Uns fehlt leider dein Geburtstag."
    end
    if not person['taufe'].empty?
      puts "Taufdatum: #{person['taufe']}"
    else
      puts "Uns fehlt leider dein Taufdatum. Kannst du dich daran erinnern?"
    end

    if not person['phone'].empty?
      puts "Festnetz: #{person['phone']}"
    end

    if not person['mobile'].empty?
      puts "Handy: #{person['mobile']}"
    end

    if not person['email'].empty?
      puts "EMail: #{person['email']}"
    end


    if not person['childrenData'].nil?
      puts "Kinder:"
      person['childrenData'].each do |child|
        if child[:birthday].nil?
          puts child[:name] + '(Geburtsdatum?)'
        else
          puts child[:name] + ' (' + child[:birthday].strftime('%d.%m.%Y') +  ')'
        end
      end
      puts ""
    end

    if not person['birthday'].empty?
        if not person['genGroups'].nil? and not  person['genGroups'].empty?
            puts "Dienste: #{person['genGroups']}"
        elsif person['age'] < 50
            puts "In welchen Diensten bist du aktuell aktiv?"
        end
    else
        puts "In welchen Diensten bist du aktuell aktiv?"
    end
    
    puts "\n\n"
  end

end
main
